//金额过滤 金额大于1取整 小于1保留
const new_price = (price) => {
	if(price > 1){
		price = Math.floor(price)
	}else{
		price = price
	}
	return price
}
//距离过滤 小于1000米显示单位m  反之显示km
const filter_distance = (distance) =>{
	let dis = distance
	if(dis < 1000){
		dis = distance + 'm'
	}
	if(dis > 1000){
		dis = Math.round(distance/1000) + 'Km'
	}
	return dis
}
//时间格式过滤器  v为时间戳  new_date为需要输出的时间格式
const date_filter = (v,new_date) => {
	let date = new Date(v * 1000); //时间戳为10位需*1000，时间戳为13位的话不需乘1000
	var Y = date.getFullYear() + '-';
	var M = (date.getMonth() + 1 < 10 ? '0' + (date.getMonth() + 1) : date.getMonth() + 1) + '-';
	var D = date.getDate() + ' ';
	var h = (date.getHours() < 10 ? '0' + date.getHours() : date.getHours()) + ':';
	var m = (date.getMinutes() < 10 ? '0' + date.getMinutes() : date.getMinutes());
	var s = date.getSeconds();
	if(new_date == "M-D h-m"){
		return M + D + h + m
	}
	if(!new_date){
		return Y + M + D + h + m + s
	}
	
}
export{
	new_price,
	date_filter,
	filter_distance
	
}
